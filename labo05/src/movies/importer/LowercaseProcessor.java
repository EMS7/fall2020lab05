//Estefan Maheux-Saban
//1931517

package movies.importer;
import java.nio.*;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	
	//constructor
	public LowercaseProcessor(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	
	//method
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> asLower = new ArrayList<String>();
		for(int i = 0; i < input.size();i++) {
			asLower.add(input.get(i).toLowerCase());
		}
		return asLower;
	}

}
