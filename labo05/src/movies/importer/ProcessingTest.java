//Estefan Maheux-Saban
//1931517
package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		String source = "C:\\Users\\Estefan\\Documents\\Working\\java310\\fall2020lab05\\fall2020lab05\\testInput";
		String output = "C:\\Users\\Estefan\\Documents\\Working\\java310\\fall2020lab05\\fall2020lab05\\testResult";
		LowercaseProcessor lcTest = new LowercaseProcessor(source, output);
		lcTest.execute();
		
		output = "C:\\Users\\Estefan\\Documents\\Working\\java310\\fall2020lab05\\fall2020lab05\\testrmduplicates";
		source = "C:\\Users\\Estefan\\Documents\\Working\\java310\\fall2020lab05\\fall2020lab05\\testResult";
		RemoveDuplicates rdTest = new RemoveDuplicates(source,output);
		rdTest.execute();
	}

}
