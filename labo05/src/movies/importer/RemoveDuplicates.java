//Estefan Maheux-Saban
//1931517

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	
	
	//constructor
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	//method
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> rmDuplicates = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			if(!rmDuplicates.contains(input.get(i))) {
				rmDuplicates.add(input.get(i));
			}
		}
		return rmDuplicates;
	}
}
